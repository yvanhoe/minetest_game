# Transforms the generic po file produced by weblate into a series of 
# locale/[modname].ja.tr files.

# TODO: make it work for all languages!

import os
import re
from collections import defaultdict

strings=dict()
modstr=defaultdict(list)
for modname in os.listdir("../mods/"):
     for fn in os.listdir("../mods/"+modname):
         if fn.endswith(".lua"):
             s=open("../mods/"+modname+"/"+fn).read()
             for gtstring in re.findall("S\(.*\)", s):
                 k=gtstring[3:-2]
                 strings[k]=modname
                 modstr[modname].append(k)
"""
for m in modstr.keys():
    print(m)
    for v in modstr[m]:
        print("\t"+v)
"""
locales = list()
for fn in os.listdir("."):
    if fn .endswith(".po"):
        locales.append(fn[:-3])
print(locales)

notused=set()
for loc in locales:
    matches = 0
    nom = 0
    msgid = None
    translation = dict()
    for line in open(loc+".po"):
        match = re.match('[^#]*msgstr[ \t]+"(.*)"', line)
        if match and msgid is not None:
            translation[msgid]=match.group(1)
        match = re.match('[^#]*msgid[ \t]+"(.*)"', line)
        if match:
            msgid = match.group(1)
            if msgid in strings:
                matches+=1
            else:
                nom+=1
                notused.add(msgid)
        else:
            msgid=None
                
    print("{}: {}/{}".format(loc, matches, matches+nom))

    for modname in os.listdir("../mods/"):
        os.makedirs("../mods/{0}/locale".format(modname), exist_ok=True)
        localefile = open("../mods/{0}/locale/{0}.{1}.tr".format(modname, loc), "w")
        localefile.write("# textdomain: "+modname+"\n\n")
        for k in modstr[modname]:
            if k in translation:
                localefile.write("{}={}\n\n".format(k,translation[k]))
        localefile.close()
print(notused)